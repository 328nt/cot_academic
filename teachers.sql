-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost:3306
-- Thời gian đã tạo: Th10 07, 2020 lúc 10:00 AM
-- Phiên bản máy phục vụ: 5.7.31-log
-- Phiên bản PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `ieg_aca`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `teachers`
--

CREATE TABLE `teachers` (
  `id` int(10) UNSIGNED NOT NULL,
  `role` int(10) UNSIGNED NOT NULL DEFAULT '3',
  `id_address` int(10) UNSIGNED DEFAULT NULL,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `teachers`
--

INSERT INTO `teachers` (`id`, `role`, `id_address`, `fullname`, `address`, `mobile`, `image`, `gender`, `dob`, `email`, `password`, `facebook`, `twitter`, `created_at`, `updated_at`, `email_verified_at`, `remember_token`) VALUES
(1, 3, 1, 'Tien Dat', '128 Nguyễn Thái học', '03444453048768', 'logo.png', NULL, '14.02.2020', 'datnt@ieg.vn', '$2y$10$I80Db//rkg9jcdiNXiE3vuWvPZIZYVzvWk7e7Ldk8Iw.WWl7g06q2', 'https://www.facebook.com/', '1', '2020-02-12 13:25:53', '2020-09-23 03:13:04', NULL, NULL),
(14, 3, 3, 'Ryan cherry', NULL, NULL, 'logo.png', 'Male', NULL, 'ryan.cherry@ieg.vn', '$2y$10$2x2E879jAM5zOqjuaFirq.Cc39UNj3oq8jcZ82zzWXDC76Pj.jfoy', NULL, NULL, '2020-08-05 00:59:06', '2020-09-06 07:26:32', NULL, NULL),
(15, 3, 3, 'Miceala O’Donovan', NULL, NULL, 'logo.png', 'Male', NULL, 'miceala.odonovan@ieg.vn', '$2y$10$0xlAiH2T7GTe/qaVHPtuQuTbKm49joVUi6eP9e62/Hpc1pd4RQweK', NULL, NULL, '2020-08-05 01:26:04', '2020-08-05 01:26:04', NULL, NULL),
(16, 3, 3, 'Alex Davidson', NULL, NULL, 'logo.png', 'Male', NULL, 'alex.davidson@ieg.vn', '$2y$10$miNpk0QskkP4bgkChwCQEOte1r6qVBVnvZij2zLlNdySNV6SZNcHS', NULL, NULL, '2020-08-05 01:27:36', '2020-08-05 01:27:36', NULL, NULL),
(17, 3, 4, 'Rivers Moore', NULL, NULL, 'logo.png', 'Male', NULL, 'rivers.moore@ieg.vn', '$2y$10$2/2.CJvGaFBMEXItbo5f9eYLUnDOZXdtObwjgRhZTfOOnQW7QLHPO', NULL, NULL, '2020-08-05 02:19:26', '2020-08-18 19:36:23', NULL, NULL),
(18, 3, 4, 'Amelia Farquharson', NULL, NULL, 'logo.png', 'Male', NULL, 'amelia.farquharson@ieg.vn', '$2y$10$75Vwi5kx3cfTVXCbFkwgI.BBHeqArlR5zIp5QVVWFeArX8yGOwf/2', NULL, NULL, '2020-08-05 02:20:00', '2020-08-05 02:20:00', NULL, NULL),
(19, 3, 4, 'elle.angeloni', NULL, NULL, 'logo.png', 'Male', NULL, 'elle.angeloni@ieg.vn', '$2y$10$tYrfOHz7M4F0XJQ8fTALR.Lyl1DeBQciQKYGR4mO2QerAjjmlY246', NULL, NULL, '2020-08-05 02:20:38', '2020-09-03 22:25:44', NULL, NULL),
(20, 3, 4, 'Nathan J. Meier', NULL, NULL, 'logo.png', 'Male', NULL, 'nathan.meier@ieg.vn', '$2y$10$PIt0vHuoii9zOIAvZhPAIukY.EoaVjWCSQcKKZZLYi5fXgNStbjey', NULL, NULL, '2020-08-05 02:21:19', '2020-08-05 02:21:19', NULL, NULL),
(21, 3, 1, 'Andrew Miech', NULL, '0397908274', 'logo.png', 'Male', NULL, 'andrew@ieg.vn', '$2y$10$wYJQI.W/TQuj8sBXEXRtA.RmWT8p0954.4Ns28inTJewhfDw2jWlK', NULL, NULL, '2020-08-12 04:48:02', '2020-08-29 02:36:00', NULL, NULL),
(22, 3, 5, 'Stephen Craddock', NULL, NULL, 'logo.png', 'Male', NULL, 'stephen.craddock@ieg.vn', '$2y$10$B6nfL0juv0MCSiGOHSWqVu6R4hJUlbJ9YstmTRxEuaVkEFVBqlMyC', NULL, NULL, '2020-08-22 19:39:36', '2020-09-23 03:14:45', NULL, NULL),
(23, 3, 5, 'Peutrus Bornman', NULL, NULL, 'logo.png', NULL, NULL, 'peutrus.bornman@ieg.vn', '$2y$10$HxCvqxuA6oNQQ4TMFM7gruQkXbOVuUuRM.K08H68okyq379pWCbti', NULL, NULL, '2020-08-27 21:48:14', '2020-08-27 21:48:14', NULL, NULL),
(24, 3, 5, 'Tratonia Spicer', NULL, NULL, 'logo.png', NULL, NULL, 'tratonia.spicer@ieg.vn', '$2y$10$f2Yw3HdnkadzbLwvjSTpmOE1kwBv1uAHW3r0LBK3Y2tXm7rdogo1O', NULL, NULL, '2020-08-30 19:19:55', '2020-08-31 09:43:06', NULL, NULL),
(25, 3, 6, 'Emma Krueger', NULL, NULL, 'logo.png', NULL, NULL, 'emma.krueger@ieg.vn', '$2y$10$2jmjuiY6Igj1lkRy2o6Aoe6WdEjP155/Cx5o/zeEW2fIvjOMfmWmW', NULL, NULL, '2020-08-31 01:33:48', '2020-08-31 01:33:48', NULL, NULL),
(26, 3, 5, 'Phạm Lan Anh', NULL, NULL, 'logo.png', 'Female', NULL, 'lananh.pham@ieg.vn', '$2y$10$qNM/JDs6BKd9tb1Kw.s9ge/srhrqg27OndJ3A0/.h9CXlHYxUH.te', NULL, NULL, '2020-09-04 01:34:01', '2020-09-04 01:34:01', NULL, NULL),
(27, 3, 5, 'Đỗ Thị Hương', NULL, NULL, 'logo.png', 'Female', NULL, 'huong.do@ieg.vn', '$2y$10$LG/YkDz05FO4oIescZ6PZOyhHFbDu2mQjAy1yYfKhto5Ww7zpKvFm', NULL, NULL, '2020-09-04 01:45:17', '2020-09-04 01:45:17', NULL, NULL),
(28, 3, 5, 'New Teacher', NULL, NULL, 'logo.png', NULL, NULL, 'newteacherdtd1@ieg.vn', '$2y$10$lvv8IsovYhOb8PVNEUvLEeNkGz.0BYPfVQDFLMJcEuqlty8ZCR.1G', NULL, NULL, '2020-09-08 01:06:39', '2020-09-08 01:06:39', NULL, NULL),
(29, 3, 1, 'Demo teacher', 'Nguyen Thai Hoc', NULL, 'logo.png', NULL, NULL, 'phuongntc@ieg.vn', '$2y$10$zQR/m5/K671ZsWPD9JFilekuY/WaWCuMKTnZ4XP9kON8an.VdBgUS', NULL, NULL, '2020-09-10 08:07:22', '2020-09-10 08:07:22', NULL, NULL),
(30, 3, 6, 'Layla Campher', NULL, NULL, 'logo.png', 'Female', NULL, 'layla.campher@ieg.vn', '$2y$10$CQKFRfRsat5RJ8T/OJc4guBxLODdA1t4Y/qvkIKRKmUXBgSlhu/km', NULL, NULL, '2020-09-14 07:30:27', '2020-09-14 07:30:27', NULL, NULL),
(31, 3, 3, 'James Williams', NULL, NULL, 'logo.png', 'Male', NULL, 'james.williams@ieg.vn', '$2y$10$xed60CGAkL3UINXN1auXBe7Nyp8FucX.VneghhE9yn4.HCuT7NxRi', NULL, NULL, '2020-09-24 07:30:56', '2020-09-24 07:30:56', NULL, NULL);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teachers_role_foreign` (`role`),
  ADD KEY `teachers_id_address_foreign` (`id_address`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `teachers`
--
ALTER TABLE `teachers`
  ADD CONSTRAINT `teachers_id_address_foreign` FOREIGN KEY (`id_address`) REFERENCES `addresses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `teachers_role_foreign` FOREIGN KEY (`role`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost:3306
-- Thời gian đã tạo: Th10 07, 2020 lúc 10:01 AM
-- Phiên bản máy phục vụ: 5.7.31-log
-- Phiên bản PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `ieg_aca`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `schedules`
--

CREATE TABLE `schedules` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_location` int(10) UNSIGNED NOT NULL,
  `id_teacher` int(10) UNSIGNED NOT NULL,
  `class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time1` date DEFAULT NULL,
  `day` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `booking` int(10) UNSIGNED DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'None',
  `author` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `schedules`
--

INSERT INTO `schedules` (`id`, `id_location`, `id_teacher`, `class`, `time`, `time1`, `day`, `created_at`, `updated_at`, `booking`, `status`, `author`) VALUES
(38, 3, 16, '2P1', '8:45 - 9:25', NULL, 'Monday', '2020-08-06 09:22:30', '2020-08-06 09:22:30', 1, 'None', 1),
(39, 3, 15, '3P1', '8:45 - 9:25', NULL, 'Monday', '2020-08-06 11:16:32', '2020-08-06 11:16:32', 1, 'None', 1),
(45, 1, 21, '1S1', '09:35 - 10:10', NULL, 'Thursday', '2020-09-09 09:26:11', '2020-09-09 09:26:11', NULL, 'None', 10),
(46, 3, 14, '4P2', '8:00 - 8:40', NULL, 'Wednesday', '2020-09-10 06:49:21', '2020-09-10 06:49:21', NULL, 'None', 12),
(50, 1, 21, '1S1', '09:35 - 10:10', NULL, 'Thursday', '2020-09-10 07:14:17', '2020-09-10 09:25:18', 12, 'None', 12),
(65, 5, 22, '5Q1 ESL', '10:20 - 11:00', NULL, 'Tuesday', '2020-09-13 11:08:41', '2020-09-17 06:02:55', 14, 'None', 14),
(66, 4, 19, '1M4', '8:00 - 8:40', NULL, 'Wednesday', '2020-09-14 07:12:37', '2020-09-14 07:26:38', 16, 'None', 16),
(67, 4, 18, '1G4', '8:00 - 8:40', NULL, 'Friday', '2020-09-14 07:34:45', '2020-09-14 07:42:59', 16, 'None', 16),
(69, 5, 23, '1Q4', '08:00 - 08:40', NULL, 'Wednesday', '2020-09-16 07:15:33', '2020-09-16 08:22:01', 12, 'None', 12),
(70, 5, 24, '4Q1 M', '10:20 - 11:00', NULL, 'Wednesday', '2020-09-16 08:45:36', '2020-09-16 09:24:35', 12, 'None', 12),
(71, 4, 20, '2M3', '14:30 - 15:00', NULL, 'Wednesday', '2020-09-17 03:07:16', '2020-09-17 03:07:16', NULL, 'None', 17),
(73, 5, 22, '5Q1 Sc', '13:30 - 14:10', NULL, 'Wednesday', '2020-09-17 06:09:33', '2020-09-17 09:41:57', 14, 'None', 14),
(74, 1, 21, '1B2', '14:45 - 15:20', NULL, 'Wednesday', '2020-09-17 08:19:36', '2020-09-17 08:42:11', 14, 'None', 14),
(75, 1, 21, '1B1', '09:40 - 10:10', NULL, 'Tuesday', '2020-09-17 08:42:54', '2020-09-17 09:13:07', 14, 'None', 14),
(76, 5, 22, '5Q3 ESL', '08:00 - 08:40', NULL, 'Friday', '2020-09-18 03:44:31', '2020-09-18 04:03:47', 12, 'None', 12),
(77, 6, 25, '3E', '15:20 - 16:00', NULL, 'Wednesday', '2020-09-18 04:48:50', '2020-09-18 05:11:09', 15, 'None', 15),
(78, 4, 19, '1P3', '8:00 - 8:40', NULL, 'Friday', '2020-09-25 04:46:50', '2020-09-25 05:02:05', 16, 'None', 16),
(79, 4, 18, '4G3', '13:45 - 14:25', NULL, 'Thursday', '2020-09-25 05:02:47', '2020-09-25 05:16:39', 16, 'None', 16),
(80, 6, 25, '3H', '14:20 - 15:00', NULL, 'Wednesday', '2020-09-28 05:15:59', '2020-09-28 05:24:35', 15, 'None', 15),
(81, 5, 22, '5Q2 ESL', '15:10 - 15:50', NULL, 'Tuesday', '2020-09-28 05:31:38', '2020-09-28 06:07:59', 14, 'None', 14),
(82, 1, 21, '1B1', '13:15 - 13:50', NULL, 'Wednesday', '2020-09-28 06:11:12', '2020-09-29 04:43:04', 14, 'None', 14),
(84, 1, 21, '1S1', '09:35 - 10:10', NULL, 'Monday', '2020-09-29 04:17:36', '2020-09-29 04:48:58', 12, 'None', 12),
(85, 3, 15, '1I', '13:45 - 14:25', NULL, 'Monday', '2020-09-29 04:18:47', '2020-09-30 09:07:54', 12, 'None', 12),
(86, 3, 14, '4M1', '9:40 - 10:20', NULL, 'Thursday', '2020-09-29 04:19:21', '2020-09-30 09:35:06', 12, 'None', 12),
(88, 4, 20, '2I3', '8:00 - 8:40', NULL, 'Wednesday', '2020-09-30 00:32:34', '2020-10-01 05:09:07', 16, 'None', 16),
(89, 3, 16, '1M2', '10:25 - 10:55', NULL, 'Thursday', '2020-09-30 09:36:12', '2020-09-30 09:46:38', 12, 'None', 12),
(90, 6, 30, '2E', '13:40 - 14:20', NULL, 'Wednesday', '2020-09-30 11:55:43', '2020-09-30 12:07:15', 15, 'None', 15),
(91, 6, 30, '2E', '13:40 - 14:20', NULL, 'Wednesday', '2020-09-30 12:08:31', '2020-09-30 12:30:39', 15, 'None', 15),
(92, 4, 20, '2I3', '8:00 - 8:40', NULL, 'Wednesday', '2020-10-01 04:57:26', '2020-10-01 04:57:26', NULL, 'None', 16),
(93, 1, 21, '1S1', '09:35 - 10:10', NULL, 'Thursday', '2020-10-01 08:38:29', '2020-10-01 08:54:40', 12, 'None', 12),
(95, 3, 16, '2G', '8:45 - 9:25', NULL, 'Thursday', '2020-10-01 10:17:17', '2020-10-01 10:37:17', 18, 'None', 18),
(96, 3, 14, '4G', '14:30 - 15:00', NULL, 'Thursday', '2020-10-01 10:45:12', '2020-10-01 11:05:32', 18, 'None', 18),
(97, 3, 15, '1I', '14:30 - 15:00', NULL, 'Monday', '2020-10-01 11:06:48', '2020-10-01 11:26:54', 18, 'None', 18),
(98, 4, 17, '3G3', '8:45 - 9:25', NULL, 'Wednesday', '2020-10-03 05:01:51', '2020-10-03 06:02:51', 12, 'None', 12),
(99, 4, 19, '1M4', '8:00 - 8:40', NULL, 'Wednesday', '2020-10-03 06:03:26', '2020-10-03 06:38:46', 12, 'None', 12),
(100, 4, 20, '2M3', '13:45 - 14:25', NULL, 'Wednesday', '2020-10-03 06:39:19', '2020-10-03 07:14:36', 12, 'None', 12),
(101, 5, 22, '5Q1 Sc', '13:30 - 14:10', NULL, 'Wednesday', '2020-10-03 06:48:24', '2020-10-03 07:14:28', 12, 'None', 12),
(102, 4, 18, '1G4', '9:45 - 10:25', NULL, 'Wednesday', '2020-10-03 07:16:19', '2020-10-03 07:57:39', 12, 'None', 12),
(103, 6, 25, '2C', '09:20 - 10:00', NULL, 'Wednesday', '2020-10-03 07:58:16', '2020-10-03 08:16:13', 12, 'None', 12),
(104, 6, 30, '2A', '10:15 - 10:55', NULL, 'Wednesday', '2020-10-03 08:16:56', '2020-10-03 08:29:11', 12, 'None', 12),
(105, 3, 31, '5M2', '14:30 - 15:00', NULL, 'Thursday', '2020-10-05 09:09:46', '2020-10-05 09:21:23', 12, 'None', 12);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `schedules_id_location_foreign` (`id_location`),
  ADD KEY `schedules_id_teacher_foreign` (`id_teacher`),
  ADD KEY `schedules_booking_foreign` (`booking`),
  ADD KEY `schedules_author_foreign` (`author`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `schedules`
--
ALTER TABLE `schedules`
  ADD CONSTRAINT `schedules_author_foreign` FOREIGN KEY (`author`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `schedules_booking_foreign` FOREIGN KEY (`booking`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `schedules_id_location_foreign` FOREIGN KEY (`id_location`) REFERENCES `addresses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `schedules_id_teacher_foreign` FOREIGN KEY (`id_teacher`) REFERENCES `teachers` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

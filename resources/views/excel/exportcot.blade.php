<style>
    .table-striped>tbody>tr:nth-child(odd)>td,
    .table-striped>tbody>tr:nth-child(odd)>th {
        background-color: #d3e1ff;
    }

    .table-striped>tbody>tr:nth-child(even)>td,
    .table-striped>tbody>tr:nth-child(even)>th {
        background-color: #fff;
    }

    .table-bordered>tbody>tr>td {
        border: 1px solid #0000 !important;
    }
</style>
<table id="customers" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th style="background-color: #b6d7a8;">id</th>
            <th style="background-color: #b6d7a8;">Ten_co_so</th>
            <th style="background-color: #b6d7a8;">Ma_lop</th>
            <th style="background-color: #b6d7a8;">Ngay_giang</th>
            <th style="background-color: #b6d7a8;">nguoi danh gia</th>
            <th style="background-color: #b6d7a8;">tk nguoi danh gia</th>
            <th style="background-color: #b6d7a8;">gv</th>
            <th style="background-color: #b6d7a8;">Email gv</th>
            <th style="background-color: #b6d7a8;">uu diem</th>
            <th style="background-color: #b6d7a8;">nhuoc diem</th>
            <th style="background-color: #b6d7a8;">Ten</th>
            <th style="background-color: #b6d7a8;">Diem</th>
            <th style="background-color: #b6d7a8;">IsDat</th>
        </tr>
    </thead>
    <tbody>
        @foreach($evaluations as $evaluation)
        <tr>
            <td>{{ $evaluation->id }}</td>
            <td>{{ $evaluation->location->name }}</td>
            <td>{{ $evaluation->schedule->class }}</td>
            <td>{{ $evaluation->schedule->time }}</td>
            <td>{{ $evaluation->user->name }}</td>
            <td>{{ $evaluation->user->email }}</td>
            <td>{{ $evaluation->teacher->fullname }}</td>
            <td>{{ $evaluation->teacher->email }}</td>
            <td>{!! $evaluation->strengths !!}</td>
            <td>{!! $evaluation->improvement !!}</td>
            <td>Part 1.1</td>
            @if ($evaluation->part1['p1_1']['basic'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part1['p1_1']['basic'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td>{{ $evaluation->id }}</td>
            <td>{{ $evaluation->location->name }}</td>
            <td>{{ $evaluation->schedule->class }}</td>
            <td>{{ $evaluation->schedule->time }}</td>
            <td>{{ $evaluation->user->name }}</td>
            <td>{{ $evaluation->user->email }}</td>
            <td>{{ $evaluation->teacher->fullname }}</td>
            <td>{{ $evaluation->teacher->email }}</td>
            <td>{!! $evaluation->strengths !!}</td>
            <td>{!! $evaluation->improvement !!}</td>
            <td>Part 1.1</td>
            @if ($evaluation->part1['p1_1']['appro'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part1['p1_1']['appro'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td>{{ $evaluation->id }}</td>
            <td>{{ $evaluation->location->name }}</td>
            <td>{{ $evaluation->schedule->class }}</td>
            <td>{{ $evaluation->schedule->time }}</td>
            <td>{{ $evaluation->user->name }}</td>
            <td>{{ $evaluation->user->email }}</td>
            <td>{{ $evaluation->teacher->fullname }}</td>
            <td>{{ $evaluation->teacher->email }}</td>
            <td>{!! $evaluation->strengths !!}</td>
            <td>{!! $evaluation->improvement !!}</td>
            <td>Part 1.1</td>
            @if ($evaluation->part1['p1_1']['compe'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part1['p1_1']['compe'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        
        <tr>
            <td>{{ $evaluation->id }}</td>
            <td>{{ $evaluation->location->name }}</td>
            <td>{{ $evaluation->schedule->class }}</td>
            <td>{{ $evaluation->schedule->time }}</td>
            <td>{{ $evaluation->user->name }}</td>
            <td>{{ $evaluation->user->email }}</td>
            <td>{{ $evaluation->teacher->fullname }}</td>
            <td>{{ $evaluation->teacher->email }}</td>
            <td>{!! $evaluation->strengths !!}</td>
            <td>{!! $evaluation->improvement !!}</td>
            <td>Part 1.2</td>
            @if ($evaluation->part1['p1_2']['basic_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part1['p1_2']['basic_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td>{{ $evaluation->id }}</td>
            <td>{{ $evaluation->location->name }}</td>
            <td>{{ $evaluation->schedule->class }}</td>
            <td>{{ $evaluation->schedule->time }}</td>
            <td>{{ $evaluation->user->name }}</td>
            <td>{{ $evaluation->user->email }}</td>
            <td>{{ $evaluation->teacher->fullname }}</td>
            <td>{{ $evaluation->teacher->email }}</td>
            <td>{!! $evaluation->strengths !!}</td>
            <td>{!! $evaluation->improvement !!}</td>
            <td>Part 1.2</td>
            @if ($evaluation->part1['p1_2']['appro_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part1['p1_2']['appro_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td>{{ $evaluation->id }}</td>
            <td>{{ $evaluation->location->name }}</td>
            <td>{{ $evaluation->schedule->class }}</td>
            <td>{{ $evaluation->schedule->time }}</td>
            <td>{{ $evaluation->user->name }}</td>
            <td>{{ $evaluation->user->email }}</td>
            <td>{{ $evaluation->teacher->fullname }}</td>
            <td>{{ $evaluation->teacher->email }}</td>
            <td>{!! $evaluation->strengths !!}</td>
            <td>{!! $evaluation->improvement !!}</td>
            <td>Part 1.2</td>
            @if ($evaluation->part1['p1_2']['compe_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part1['p1_2']['compe_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td>{{ $evaluation->id }}</td>
            <td>{{ $evaluation->location->name }}</td>
            <td>{{ $evaluation->schedule->class }}</td>
            <td>{{ $evaluation->schedule->time }}</td>
            <td>{{ $evaluation->user->name }}</td>
            <td>{{ $evaluation->user->email }}</td>
            <td>{{ $evaluation->teacher->fullname }}</td>
            <td>{{ $evaluation->teacher->email }}</td>
            <td>{!! $evaluation->strengths !!}</td>
            <td>{!! $evaluation->improvement !!}</td>
            <td>Part 1.2</td>
            @if ($evaluation->part1['p1_2']['outst_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part1['p1_2']['outst_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td>{{ $evaluation->id }}</td>
            <td>{{ $evaluation->location->name }}</td>
            <td>{{ $evaluation->schedule->class }}</td>
            <td>{{ $evaluation->schedule->time }}</td>
            <td>{{ $evaluation->user->name }}</td>
            <td>{{ $evaluation->user->email }}</td>
            <td>{{ $evaluation->teacher->fullname }}</td>
            <td>{{ $evaluation->teacher->email }}</td>
            <td>{!! $evaluation->strengths !!}</td>
            <td>{!! $evaluation->improvement !!}</td>
            <td>Part 1.2</td>
            @if ($evaluation->part1['p1_2']['appro_2'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part1['p1_2']['appro_2'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td>{{ $evaluation->id }}</td>
            <td>{{ $evaluation->location->name }}</td>
            <td>{{ $evaluation->schedule->class }}</td>
            <td>{{ $evaluation->schedule->time }}</td>
            <td>{{ $evaluation->user->name }}</td>
            <td>{{ $evaluation->user->email }}</td>
            <td>{{ $evaluation->teacher->fullname }}</td>
            <td>{{ $evaluation->teacher->email }}</td>
            <td>{!! $evaluation->strengths !!}</td>
            <td>{!! $evaluation->improvement !!}</td>
            <td>Part 1.2</td>
            @if ($evaluation->part1['p1_2']['compe_2'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part1['p1_2']['compe_2'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td>{{ $evaluation->id }}</td>
            <td>{{ $evaluation->location->name }}</td>
            <td>{{ $evaluation->schedule->class }}</td>
            <td>{{ $evaluation->schedule->time }}</td>
            <td>{{ $evaluation->user->name }}</td>
            <td>{{ $evaluation->user->email }}</td>
            <td>{{ $evaluation->teacher->fullname }}</td>
            <td>{{ $evaluation->teacher->email }}</td>
            <td>{!! $evaluation->strengths !!}</td>
            <td>{!! $evaluation->improvement !!}</td>
            <td>Part 1.2</td>
            @if ($evaluation->part1['p1_2']['outst_2'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part1['p1_2']['outst_2'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>

        <tr>
            <td>{{ $evaluation->id }}</td>
            <td>{{ $evaluation->location->name }}</td>
            <td>{{ $evaluation->schedule->class }}</td>
            <td>{{ $evaluation->schedule->time }}</td>
            <td>{{ $evaluation->user->name }}</td>
            <td>{{ $evaluation->user->email }}</td>
            <td>{{ $evaluation->teacher->fullname }}</td>
            <td>{{ $evaluation->teacher->email }}</td>
            <td>{!! $evaluation->strengths !!}</td>
            <td>{!! $evaluation->improvement !!}</td>
            <td>Part 2A1.</td>
            @if ($evaluation->part2a['p2a1']['basic'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2a['p2a1']['basic'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td>{{ $evaluation->id }}</td>
            <td>{{ $evaluation->location->name }}</td>
            <td>{{ $evaluation->schedule->class }}</td>
            <td>{{ $evaluation->schedule->time }}</td>
            <td>{{ $evaluation->user->name }}</td>
            <td>{{ $evaluation->user->email }}</td>
            <td>{{ $evaluation->teacher->fullname }}</td>
            <td>{{ $evaluation->teacher->email }}</td>
            <td>{!! $evaluation->strengths !!}</td>
            <td>{!! $evaluation->improvement !!}</td>
            <td>Part 2A1.</td>
            @if ($evaluation->part2a['p2a1']['appro'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2a['p2a1']['appro'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td>{{ $evaluation->id }}</td>
            <td>{{ $evaluation->location->name }}</td>
            <td>{{ $evaluation->schedule->class }}</td>
            <td>{{ $evaluation->schedule->time }}</td>
            <td>{{ $evaluation->user->name }}</td>
            <td>{{ $evaluation->user->email }}</td>
            <td>{{ $evaluation->teacher->fullname }}</td>
            <td>{{ $evaluation->teacher->email }}</td>
            <td>{!! $evaluation->strengths !!}</td>
            <td>{!! $evaluation->improvement !!}</td>
            <td>Part 2A2.</td>
            @if ($evaluation->part2a['p2a2']['basic'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2a['p2a2']['basic'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td>{{ $evaluation->id }}</td>
            <td>{{ $evaluation->location->name }}</td>
            <td>{{ $evaluation->schedule->class }}</td>
            <td>{{ $evaluation->schedule->time }}</td>
            <td>{{ $evaluation->user->name }}</td>
            <td>{{ $evaluation->user->email }}</td>
            <td>{{ $evaluation->teacher->fullname }}</td>
            <td>{{ $evaluation->teacher->email }}</td>
            <td>{!! $evaluation->strengths !!}</td>
            <td>{!! $evaluation->improvement !!}</td>
            <td>Part 2A2.</td>
            @if ($evaluation->part2a['p2a2']['appro'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2a['p2a2']['appro'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td>{{ $evaluation->id }}</td>
            <td>{{ $evaluation->location->name }}</td>
            <td>{{ $evaluation->schedule->class }}</td>
            <td>{{ $evaluation->schedule->time }}</td>
            <td>{{ $evaluation->user->name }}</td>
            <td>{{ $evaluation->user->email }}</td>
            <td>{{ $evaluation->teacher->fullname }}</td>
            <td>{{ $evaluation->teacher->email }}</td>
            <td>{!! $evaluation->strengths !!}</td>
            <td>{!! $evaluation->improvement !!}</td>
            <td>Part 2A2.</td>
            @if ($evaluation->part2a['p2a2']['compe'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2a['p2a2']['compe'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td>{{ $evaluation->id }}</td>
            <td>{{ $evaluation->location->name }}</td>
            <td>{{ $evaluation->schedule->class }}</td>
            <td>{{ $evaluation->schedule->time }}</td>
            <td>{{ $evaluation->user->name }}</td>
            <td>{{ $evaluation->user->email }}</td>
            <td>{{ $evaluation->teacher->fullname }}</td>
            <td>{{ $evaluation->teacher->email }}</td>
            <td>{!! $evaluation->strengths !!}</td>
            <td>{!! $evaluation->improvement !!}</td>
            <td>Part 2A2.</td>
            @if ($evaluation->part2a['p2a2']['outst'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2a['p2a2']['outst'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 2A3.</td>
            @if ($evaluation->part2a['p2a3']['basic'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2a['p2a3']['basic'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 2A3.</td>
            @if ($evaluation->part2a['p2a3']['appro'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2a['p2a3']['appro'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 2A3.</td>
            @if ($evaluation->part2a['p2a3']['compe'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2a['p2a3']['compe'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        
        
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 2b1.</td>
            @if ($evaluation->part2b['p2b1']['basic'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2b['p2b1']['basic'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 2b1.</td>
            @if ($evaluation->part2b['p2b1']['appro'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2b['p2b1']['appro'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        
        
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 2b2.</td>
            @if ($evaluation->part2b['p2b2']['basic'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2b['p2b2']['basic'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 2b2.</td>
            @if ($evaluation->part2b['p2b2']['appro'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2b['p2b2']['appro'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 2b2.</td>
            @if ($evaluation->part2b['p2b2']['compe'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2b['p2b2']['compe'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 2b2.</td>
            @if ($evaluation->part2b['p2b2']['outst'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2b['p2b2']['outst'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 2c1.</td>
            @if ($evaluation->part2c['p2c1']['basic'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2c['p2c1']['basic'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 2c1.</td>
            @if ($evaluation->part2c['p2c1']['appro'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2c['p2c1']['appro'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 2c1.</td>
            @if ($evaluation->part2c['p2c1']['compe'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2c['p2c1']['compe'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 2c1.</td>
            @if ($evaluation->part2c['p2c1']['outst'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2c['p2c1']['outst'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        
        
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 2c2.</td>
            @if ($evaluation->part2c['p2c2']['basic_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2c['p2c2']['basic_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 2c2.</td>
            @if ($evaluation->part2c['p2c2']['appro_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2c['p2c2']['appro_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 2c2.</td>
            @if ($evaluation->part2c['p2c2']['compe_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2c['p2c2']['compe_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 2c2.</td>
            @if ($evaluation->part2c['p2c2']['outst_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2c['p2c2']['outst_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 2c2.</td>
            @if ($evaluation->part2c['p2c2']['basic_2'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2c['p2c2']['basic_2'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        
        
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 2c3.</td>
            @if ($evaluation->part2c['p2c3']['basic'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2c['p2c3']['basic'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 2c3.</td>
            @if ($evaluation->part2c['p2c3']['appro'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2c['p2c3']['appro'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 2c3.</td>
            @if ($evaluation->part2c['p2c3']['compe'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part2c['p2c3']['compe'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3a1.</td>
            @if ($evaluation->part3a['p3a1']['basic'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3a['p3a1']['basic'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3a1.</td>
            @if ($evaluation->part3a['p3a1']['appro'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3a['p3a1']['appro'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3a2.</td>
            @if ($evaluation->part3a['p3a2']['basic'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3a['p3a2']['basic'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3a2.</td>
            @if ($evaluation->part3a['p3a2']['appro'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3a['p3a2']['appro'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3a2.</td>
            @if ($evaluation->part3a['p3a2']['compe'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3a['p3a2']['compe'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3a2.</td>
            @if ($evaluation->part3a['p3a2']['outst'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3a['p3a2']['outst'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3a3.</td>
            @if ($evaluation->part3a['p3a3']['basic'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3a['p3a3']['basic'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3a3.</td>
            @if ($evaluation->part3a['p3a3']['appro'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3a['p3a3']['appro'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3a3.</td>
            @if ($evaluation->part3a['p3a3']['compe'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3a['p3a3']['compe'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3a3.</td>
            @if ($evaluation->part3a['p3a3']['outst'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3a['p3a3']['outst'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3a4.</td>
            @if ($evaluation->part3a['p3a4']['basic_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3a['p3a4']['basic_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3a4.</td>
            @if ($evaluation->part3a['p3a4']['appro_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3a['p3a4']['appro_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3a4.</td>
            @if ($evaluation->part3a['p3a4']['compe_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3a['p3a4']['compe_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3a4.</td>
            @if ($evaluation->part3a['p3a4']['outst_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3a['p3a4']['outst_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3a4.</td>
            @if ($evaluation->part3a['p3a4']['basic_2'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3a['p3a4']['basic_2'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3a4.</td>
            @if ($evaluation->part3a['p3a4']['appro_2'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3a['p3a4']['appro_2'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3a4.</td>
            @if ($evaluation->part3a['p3a4']['compe_2'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3a['p3a4']['compe_2'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3a4.</td>
            @if ($evaluation->part3a['p3a4']['outst_2'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3a['p3a4']['outst_2'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>

        
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3b1.</td>
            @if ($evaluation->part3b['p3b1']['basic_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3b['p3b1']['basic_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3b1.</td>
            @if ($evaluation->part3b['p3b1']['appro_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3b['p3b1']['appro_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3b1.</td>
            @if ($evaluation->part3b['p3b1']['compe_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3b['p3b1']['compe_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3b1.</td>
            @if ($evaluation->part3b['p3b1']['outst_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3b['p3b1']['outst_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>

        
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3b1.</td>
            @if ($evaluation->part3b['p3b1']['basic_2'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3b['p3b1']['basic_2'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3b1.</td>
            @if ($evaluation->part3b['p3b1']['appro_2'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3b['p3b1']['appro_2'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3b1.</td>
            @if ($evaluation->part3b['p3b1']['compe_2'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3b['p3b1']['compe_2'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3b1.</td>
            @if ($evaluation->part3b['p3b1']['outst_2'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3b['p3b1']['outst_2'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>

        
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3b2.</td>
            @if ($evaluation->part3b['p3b2']['basic'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3b['p3b2']['basic'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3b2.</td>
            @if ($evaluation->part3b['p3b2']['appro'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3b['p3b2']['appro'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3b2.</td>
            @if ($evaluation->part3b['p3b2']['compe'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3b['p3b2']['compe'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>

        

        
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3c1.</td>
            @if ($evaluation->part3c['p3c1']['basic_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3c['p3c1']['basic_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3c1.</td>
            @if ($evaluation->part3c['p3c1']['appro_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3c['p3c1']['appro_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3c1.</td>
            @if ($evaluation->part3c['p3c1']['compe_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3c['p3c1']['compe_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3c1.</td>
            @if ($evaluation->part3c['p3c1']['outst_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3c['p3c1']['outst_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3c1.</td>
            @if ($evaluation->part3c['p3c1']['basic_2'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3c['p3c1']['basic_2'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>

        
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3c2.</td>
            @if ($evaluation->part3c['p3c2']['basic'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3c['p3c2']['basic'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3c2.</td>
            @if ($evaluation->part3c['p3c2']['appro'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3c['p3c2']['appro'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3c2.</td>
            @if ($evaluation->part3c['p3c2']['compe'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3c['p3c2']['compe'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3c2.</td>
            @if ($evaluation->part3c['p3c2']['outst'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3c['p3c2']['outst'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>

        
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3d1.</td>
            @if ($evaluation->part3d['p3d1']['basic'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3d['p3d1']['basic'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3d1.</td>
            @if ($evaluation->part3d['p3d1']['appro'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3d['p3d1']['appro'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3d1.</td>
            @if ($evaluation->part3d['p3d1']['compe'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3d['p3d1']['compe'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3d1.</td>
            @if ($evaluation->part3d['p3d1']['outst'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3d['p3d1']['outst'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>

        
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3d2.</td>
            @if ($evaluation->part3d['p3d2']['basic_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3d['p3d2']['basic_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3d2.</td>
            @if ($evaluation->part3d['p3d2']['appro_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3d['p3d2']['appro_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3d2.</td>
            @if ($evaluation->part3d['p3d2']['compe_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3d['p3d2']['compe_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3d2.</td>
            @if ($evaluation->part3d['p3d2']['outst_1'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3d['p3d2']['outst_1'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>

        
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3d2.</td>
            @if ($evaluation->part3d['p3d2']['basic_2'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3d['p3d2']['basic_2'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3d2.</td>
            @if ($evaluation->part3d['p3d2']['appro_2'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3d['p3d2']['appro_2'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3d2.</td>
            @if ($evaluation->part3d['p3d2']['compe_2'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3d['p3d2']['compe_2'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3d2.</td>
            @if ($evaluation->part3d['p3d2']['outst_2'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3d['p3d2']['outst_2'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>

        
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3d2.</td>
            @if ($evaluation->part3d['p3d2']['basic_3'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3d['p3d2']['basic_3'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3d2.</td>
            @if ($evaluation->part3d['p3d2']['appro_3'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3d['p3d2']['appro_3'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 3d2.</td>
            @if ($evaluation->part3d['p3d2']['compe_3'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part3d['p3d2']['compe_3'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>

        
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 4a1.</td>
            @if ($evaluation->part4a['p4a1']['basic'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part4a['p4a1']['basic'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 4a1.</td>
            @if ($evaluation->part4a['p4a1']['appro'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part4a['p4a1']['appro'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 4a1.</td>
            @if ($evaluation->part4a['p4a1']['compe'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part4a['p4a1']['compe'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Part 4a1.</td>
            @if ($evaluation->part4a['p4a1']['outst'] == null)
            <td>0</td>
            <td>FALSE</td>
            @else
            <td>{{ $evaluation->part4a['p4a1']['outst'] }}</td>
            <td>TRUE</td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>
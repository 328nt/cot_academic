<div class="header-top-area" style="left: 0px">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="header-top-wraper">
                    <div class="row">
                        <div class="col-lg-1 col-md-0 col-sm-1 col-xs-12">
                            <div class="menu-switcher-pro">
                                <button type="button" id="sidebarCollapse"
                                    class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
                                    <i class="educate-icon educate-nav"></i>
                                </button>
                            </div>
                        </div>

                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <div class="header-top-menu tabl-d-n">
                                <ul class="nav navbar-nav mai-top-nav">
                                    <li
                                        class="nav-item {{ request()->route()->named('teacher_index*') ? 'current' : '' }}">
                                        <a href="{{route('teacher_index')}}" class="nav-link">Lesson plan
                                            submission</a>
                                    </li>
                                    {{-- <li
                                        class="nav-item {{ request()->is('admin/schedules/booking*') ? 'current' : '' }}">
                                    <a href="admin/schedules/booking" class="nav-link">Schedule</a>
                                    </li> --}}
                                    <li class="nav-item {{ request()->route()->named('evaluated*') ? 'current' : '' }}">
                                        <a href="{{route('evaluated')}}" class="nav-link">Evaluated</a>
                                    </li>
                                    <li
                                        class="nav-item {{ request()->is('admin/evaluation/draft*') ? 'current' : '' }}">
                                        <a href="admin/evaluation/draft" class="nav-link">Draft</a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div class="header-right-info">
                                <ul class="nav navbar-nav mai-top-nav header-right-menu">

                                    <li class="nav-item">
                                        <a href="#" data-toggle="dropdown" role="button" aria-expanded="false"
                                            class="nav-link dropdown-toggle">
                                            <img src="upload/teachers/{{Auth::guard('teacher')->user()->image}}"
                                                alt="" />
                                            <span class="admin-name">{{Auth::guard('teacher')->user()->fullname}}</span>
                                            <i class="fa fa-angle-down edu-icon edu-down-arrow"></i>
                                        </a>
                                        <ul role="menu"
                                            class="dropdown-header-top author-log dropdown-menu animated zoomIn">
                                            <li><a
                                                    href="{{route('teacher_profile', Auth::guard('teacher')->user()->id)}}"><span
                                                        class="edu-icon edu-user-rounded author-log-ic"></span>My
                                                    Profile</a>
                                            </li>
                                            <li><a href="{{route('teacher_logout')}}"><span
                                                        class="edu-icon edu-locked author-log-ic"></span>Log
                                                    Out</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('msg')
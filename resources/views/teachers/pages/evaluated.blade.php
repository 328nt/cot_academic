@extends('teachers/layouts/index')
@section('title')
Index
@endsection
@section('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
<!-- Bootstrap CSS
		============================================ -->
<link rel="stylesheet" href="be/css/bootstrap.min.css">
<!-- modals CSS
		============================================ -->
<link rel="stylesheet" href="be/css/modals.css">
<!-- style CSS
		============================================ -->
<link rel="stylesheet" href="be/style.css">
@endsection
@section('content')
<div class="breadcome-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcome-list single-page-breadcome">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcome-heading">
                                <form role="search" class="sr-input-func">
                                    <input type="text" placeholder="Search..." class="search-int form-control">
                                    <a href="#"><i class="fa fa-search"></i></a>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="text-align: center">

                        <h2>IEG - School Partner<br>Lesson plan submission</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="library-book-area mg-t-30">
    <div class="container-fluid">
        <div class="row">
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Class</th>
                        <th>Time/Day</th>
                        <th>QC</th>
                        <th>Score</th>
                        <th>Info</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($evaluated as $evaluation)
                    <tr>
                        <td>
                            {{$evaluation->test}}
                        </td>
                        <td>
                            {{$evaluation->schedule->class}}
                        </td>
                        <td>
                            {{$evaluation->schedule->time}}
                            /
                            {{$evaluation->schedule->day}}
                        </td>
                        <td>
                            {{$evaluation->user->name}}
                        </td>
                        <td>
                            {{$total =  
                                array_sum($evaluation->part1['p1_1']) + array_sum($evaluation->part1['p1_2'])+
                                array_sum($evaluation->part2a['p2a1'])+array_sum($evaluation->part2a['p2a2'])+array_sum($evaluation->part2a['p2a3'])+
                                array_sum($evaluation->part2b['p2b1'])+array_sum($evaluation->part2b['p2b2'])+
                                array_sum($evaluation->part2c['p2c1'])+array_sum($evaluation->part2c['p2c2'])+array_sum($evaluation->part2c['p2c3'])+
                                array_sum($evaluation->part2d['p2d1'])+array_sum($evaluation->part2d['p2d2'])+
                                array_sum($evaluation->part3a['p3a1'])+array_sum($evaluation->part3a['p3a2'])+array_sum($evaluation->part3a['p3a3'])+array_sum($evaluation->part3a['p3a4'])+
                                array_sum($evaluation->part3b['p3b1'])+array_sum($evaluation->part3b['p3b2'])+
                                array_sum($evaluation->part3c['p3c1'])+array_sum($evaluation->part3c['p3c2'])+
                                array_sum($evaluation->part3d['p3d1'])+array_sum($evaluation->part3d['p3d2'])+
                                array_sum($evaluation->part4a['p4a1'])+array_sum($evaluation->part4a['p4a2'])
                                            }}
                        </td>
                        <td>
                            <a href="{{route('info_evaluation', $evaluation->id)}}">Info</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Date</th>
                        <th>Class</th>
                        <th>Time/Day</th>
                        <th>QC</th>
                        <th>Score</th>
                        <th>Info</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

@endsection
@section('script2')
<script>
    $(document).ready(function() {
    $('#example').DataTable();
} );
</script>

<!-- jquery
		============================================ -->
<script src="be/js/vendor/jquery-1.12.4.min.js"></script>
<!-- bootstrap JS
        ============================================ -->
<script src="be/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
@endsection
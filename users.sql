-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost:3306
-- Thời gian đã tạo: Th10 07, 2020 lúc 10:04 AM
-- Phiên bản máy phục vụ: 5.7.31-log
-- Phiên bản PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `ieg_aca`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `role`, `image`, `mobile`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'dat', 'datnt@ieg.vn', 1, 'default.png', '0344445304', NULL, '$2y$10$JLfUiJAzhXavGtNhCASv/OAOcYxYnaRHahcKwcczUVDrVmdv8PjP6', NULL, '2020-02-10 20:39:12', '2020-02-11 04:27:33'),
(10, 'Bạch Mỹ Hạnh', 'hanh.bach@ieg.vn', 1, 'default.png', NULL, NULL, '$2y$10$gJGDfDH3hyhtZKY49IA5xOg6BXx.7V6h3OGM2X/.QDZCtJNwX3KMy', NULL, '2020-09-01 04:43:15', '2020-09-01 04:43:15'),
(11, 'evaluator', 'evaluator@ieg.vn', 2, 'default.png', '0', NULL, '$2y$10$Q8EcpYC8CNwoIR44dMw/n.9ApOMJELzT268MwvDtFe9xh1pTgull6', NULL, '2020-09-01 10:16:01', '2020-09-01 10:16:01'),
(12, 'Academic', 'academic@ieg.vn', 1, 'default.png', NULL, NULL, '$2y$10$kdlMIDZRi11Sn0ahDWkEyOnUtgW6.D/4RUcSa8PSpkNe5a3bUeDV6', NULL, '2020-09-04 08:41:31', '2020-09-04 08:41:31'),
(14, 'Tratonia Spicer', 'tratonia.spicer@ieg.vn', 2, 'default.png', NULL, NULL, '$2y$10$0qNL.t.UyAysEN5lQ6bpaOJBF3OuYbug..kUOwmtu.HnWa3SGYhdq', NULL, '2020-09-04 11:03:56', '2020-09-04 11:03:56'),
(15, 'Peutrus Bornman', 'peutrus.bornman@ieg.vn', 2, 'default.png', NULL, NULL, '$2y$10$b0uCtWvS8GflJ6.mQ9vp7uN4U/CUkSa7ra655.IjhOqIEGYNa9FJK', NULL, '2020-09-04 11:05:45', '2020-09-04 11:05:45'),
(16, 'Rivers Moore', 'rivers.moore@ieg.vn', 2, 'default.png', NULL, NULL, '$2y$10$PojHlQDXMPREBVU9zvVDWOOJwme4PbEsCcgWmvqqHEh/2QodutXTy', NULL, '2020-09-04 11:07:12', '2020-09-04 11:07:12'),
(17, 'Nguyễn Thị Cẩm Phương', 'phuongntc@ieg.vn', 2, 'default.png', '0904824890', NULL, '$2y$10$3YE7QTEHrlRt6p/eD7fRIuP8Uocp1SamBVighEly8Z6ocUJYsT73m', NULL, '2020-09-10 06:51:17', '2020-09-10 06:51:17'),
(18, 'James William', 'james.williams@ieg.vn', 1, 'default.png', NULL, NULL, '$2y$10$OhtmhJb3xTLyf.Le2xbIeO8cfyA2K6ddM8zHubZe73D38YTUvl1.q', NULL, '2020-09-24 07:33:55', '2020-09-24 07:33:55');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_foreign` (`role`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_foreign` FOREIGN KEY (`role`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
